# Taches

## Cote Client (Android / IOS / Front-end)

* Connexion / Inscription

Tache | Temps
----- | -----
Design | 2JH
Developpement | 2JH

* Onboarding

Tache | Temps
----- | -----
Design | 1JH
Developpement | 0.5JH

* Home Page

Tache | Temps
----- | -----
Design | 1JH
Developpement | 1.5JH

* Page SSH

Tache | Temps
----- | -----
Design | 1JH
Developpement | 3JH

* Page FTP

Tache | Temps
----- | -----
Design | 1.5JH
Developpement | 5JH

* Profil

Tache | Temps
----- | -----
Design | 1JH
Developpement | 0.5JH

* Options

Tache | Temps
----- | -----
Design | 1JH
Developpement | 1JH

## Cote Backend / API

* Utilisateurs / authentification

Tache | Temps
----- | -----
DB | 1JH
Developpement | 1JH

* Informations de connexion FTP / SSH (enregistrement)

Tache | Temps
----- | -----
DB | 1JH
Developpement | 1.5JH

## Modules (cote client)

* Monitoring serveur

Tache | Temps
----- | -----
Design | 1JH
Developpement | 1.5JH

* Snippet lors de la connexion

Tache | Temps
----- | -----
Design | 0.5JH
Developpement | 1JH

* Recuperation "automatique" de fichiers via FTP

Tache | Temps
----- | -----
Design | 1JH
Developpement | 2JH