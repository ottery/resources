#HSLIDE

## <span style="color: #00aaff">Ottery</span>

#### Présentation
<br>
<span style="color:gray">Présenté par Quentin, Soomin et Fabien</span>

#HSLIDE

### Présentation du projet <span style="color: #00aaff">Ottery</span>
<br>
<span style="color:gray; font-size:0.6em;">[ Client FTP & SSH ]</span>

#HSLIDE

### ANDROID
<br>
<span style="color:gray; font-size:0.6em;">[ Par Quentin Richard ]</span>

#HSLIDE

### ANDROID - fonctionnalités

#### <span class="fragment" data-fragment-index="1"><span style="color: #666666">Visuel</span>
#### <span class="fragment" data-fragment-index="2"><span style="color: #666666">Login</span>

#HSLIDE

### iOS

<br>
<span style="color:gray; font-size:0.6em;">[ Soomin Lee ]</span>

#HSLIDE

### iOS - Features

#### <span class="fragment" data-fragment-index="1">Connexion/Inscription</span>
#### <span class="fragment" data-fragment-index="2">FTP</span>
#### <span class="fragment" data-fragment-index="3">SSH</span>
#### <span class="fragment" data-fragment-index="4">Profil simple</span>

#HSLIDE

### Application WEB
<br>
<span style="color:gray; font-size:0.6em;">[ Par Fabien Martinez ]</span>

#HSLIDE

### Application WEB - Features
<br>
#### <span class="fragment" data-fragment-index="1">Création et gestion d'un compte <span style="color: #666666">Utilisateur</span>
#### <span class="fragment" data-fragment-index="2">Gestion des <span style="color: #00aaff">Connections</span>

#HSLIDE

### API
<br>
<span style="color:gray; font-size:0.6em;">[ Par Fabien Martinez ]</span>

#HSLIDE

### API - Features
<br>
#### <span class="fragment" data-fragment-index="1">Gestion des <span style="color: #666666">Utilisateurs</span>
#### <span class="fragment" data-fragment-index="2">Gestion des <span style="color: #666666">Connections</span>
#### <span class="fragment" data-fragment-index="3">Utilisation de <span style="color: #00aaff">Slate</span> pour la <span style="color: #00aaff">documentation</span>

#HSLIDE

### Problèmes rencontrés

#HSLIDE

## <span style="color: #00aaff">Conclusion</span>

#HSLIDE
<span style="color: #00aaff">Conclusion</span>
